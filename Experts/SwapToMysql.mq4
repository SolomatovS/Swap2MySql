//+------------------------------------------------------------------+
//|                                                  SwapToMysql.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <EAX_Mysql.mqh>
#include <stderror.mqh>
#import "user32.dll"
   int GetParent (int hWnd);
   int PostMessageA (int hWnd, int Msg, int wParam, int lParam);
#import

#include <WinUser32.mqh>

// global
EAX_Mysql *db = NULL;
string  host     = "localhost";
string  user     = "swapsolution";
string  pass     = "swapsolution";
string  dbName   = "swapsolution";

int     port     = 3306;
int     socket   = 0;
int     client   = 0;

int     dbConnectId = 0;
bool    isConnect = false;
long    ChartIDExpert;

extern int      PeriodUpdateSwap = 60 * 60; // ���: �������� �����
//extern int      PeriodUpdateQuote = 60;  // ������ ���������� ��������� ������������
extern int      SleepOpenCharts = 2000;   // ��.���: ��������� �������� ���� �������

struct AccountModel
{
   int id;
   int Login;
   string Company;
   string Currency;
   string Name;
   string Server;
   int AccountType;
   int Leverage;
   int MarginSOMode;
   double SOLevel;
};

struct InstrumentModel
{
   int id;
   int id_accounts;
   string Instrument;
   int isMarketWatch;
   string BaseAsset;
   string MarginAsset;
   string ProfitAsset;
   string SymbolDescription;
   long TypeSwap;             // ������ ������� �����
   double SwapBuy;            // ���� �� Buy
   double SwapSell;           // ���� �� Sell
   double SwapBuyUSD;
   double SwapSellUSD;
   double ContractSize;
   double MarginSymbolUSD;
   int digits;
   int TradeMode;
   double LastBid;
   double LastAsk;
   int Spread;
   int LastUpdate;
   double SpreadUSD;
   double Volatility;
   double Profitability;
   double VolatilityUSD;
   double ProfitabilityUSD;
};

AccountModel AccountDB[];

void OnInit()
{
   if (db != NULL)
   {
      OnDeinit(0);
   }
   db = new EAX_Mysql();
   ConnectMySql();
   
   ChartIDExpert = ChartID();
   
   EventSetTimer(PeriodUpdateSwap * 60);
   OnTimer();
}

void OnDeinit(const int reason)
{
   // clean up memory
   delete db;
   EventKillTimer();
}

void OnTimer()
{
   InstrumentChecking();
   InstrumentFloatingParametrsUpdate();
   /*
   static int lastUpdate;
   if ((lastUpdate - TimeLocal()) > PeriodUpdateQuote * 60)
   {
      InstrumentFloatingParametrsUpdate();
      lastUpdate = TimeLocal();
   }*/
}

void ConnectMySql()
{
   if (isConnect) return;
   db.connect(host, user, pass, dbName);
   if (db.getLastErrorNumber() == 0)
   {
      isConnect = true;
   }
}

void MTGetAccount(AccountModel &account)
{
   account.id           = 0;
   account.Login        = AccountNumber();
   account.Company      = AccountCompany();
   account.Currency     = AccountCurrency();
   account.Name         = AccountName();
   account.Server       = AccountServer();
   account.AccountType  = AccountInfoInteger(ACCOUNT_TRADE_MODE);
   account.Leverage     = AccountInfoInteger(ACCOUNT_LEVERAGE);
   account.MarginSOMode = AccountInfoInteger(ACCOUNT_MARGIN_SO_MODE);
   account.SOLevel      = AccountInfoDouble(ACCOUNT_MARGIN_SO_SO);
}

void DBGetAccount(AccountModel &account, uint i)
{
   account.id           = (int)     db.get("id", i);
   account.Login        = (int)     db.get("Login", i);
   account.Company      = (string)  db.get("Company", i);
   account.Currency     = (string)  db.get("Currency", i);
   account.Name         = (string)  db.get("Name", i);
   account.Server       = (string)  db.get("Server", i);
   account.AccountType  = (int)     db.get("AccountType", i);
   account.Leverage     = (int)     db.get("Leverage", i);
   account.MarginSOMode = (int)     db.get("MarginSOMode", i);
   account.SOLevel      = (double)  db.get("SOLevel", i);

}

string DBWhereAccountModel(AccountModel &account)
{
   string where = StringConcatenate(
      "`Login` = '",        account.Login, "' ",
      "AND `Company` = '",  account.Company, "' ",
      "AND `Currency` = '", account.Currency, "' ",
      "AND `Name` = '",     account.Name, "' ",
      "AND `Server` = '",   account.Server, "'");
   
   return where;
}

uint DBSearchAccount(AccountModel &account)
{
   db.select("accounts");
   // ������ ������ � account
   string where = DBWhereAccountModel(account);
   uint num_row = db.readWhere(where);
   if (num_row > 0)
   {
      DBGetAccount(account, 0);
   }
   
   return num_row;
}

void DBSetAccount(AccountModel &account)
{
   db.set("Login",      IntegerToString(account.Login));
   db.set("Company",    account.Company);
   db.set("Currency",   account.Currency);
   db.set("Name",       account.Name);
   db.set("Server",     account.Server);
   db.set("AccountType",account.AccountType);
   db.set("Leverage",   account.Leverage);
   db.set("MarginSOMode", account.MarginSOMode);
   db.set("SOLevel",    account.SOLevel);
}
void DBUpdateAccount(uint id_account, AccountModel &account)
{
   db.select("accounts");
   // ������ ������ � ������ id_account
   db.read(StringConcatenate(id_account));
   // �������� �������� ���� ������
   DBSetAccount(account);
   // ��������� ������ (��������������)
   db.write();
}
void DBInsertAccount(AccountModel &account)
{
   // ��������� ����� ������ � ����
   db.AddNew("accounts");
   // ��������� �������� ���� ������
   DBSetAccount(account);
   // ���������� ������ � ����
   db.write();
}
bool isConnected()
{
   string Log;
   bool isConneted = IsConnected();
   if (!isConneted)
   {
      Log = StringConcatenate("����������� ����� � ��������, ��������� ������ ����������� � ������� �����");
      Alert(Log);
   }
   return isConneted;
}

bool CompareAccountParametrs(AccountModel &accountInMT, AccountModel &accountInDB)
{
   string Log;
   bool isEquivalent = true;
   // ���������� AccountType
   if (accountInMT.AccountType != accountInDB.AccountType)
   {
      isEquivalent = false;
      Log = StringConcatenate("AccountType: [", accountInMT.Login, "][", accountInMT.AccountType, "] != [", accountInDB.Login, "][", accountInDB.AccountType, "]");
      Alert(Log);
   }
   // ���������� MarginSOMode
   if (accountInMT.MarginSOMode != accountInDB.MarginSOMode)
   {
      isEquivalent = false;
      Log = StringConcatenate("MarginSOMode: [", accountInMT.Login, "][", accountInMT.MarginSOMode, "] != [", accountInDB.Login, "][", accountInDB.MarginSOMode, "]");
      Alert(Log);
   }
   // ���������� SOLevel
   if (accountInMT.SOLevel != accountInDB.SOLevel)
   {
      isEquivalent = false;
      Log = StringConcatenate("SOLevel: [", accountInMT.Login, "][", accountInMT.SOLevel, "] != [", accountInDB.Login, "][", accountInDB.SOLevel, "]");
      Alert(Log);
   }
   // ���������� Leverage
   if (accountInMT.Leverage != accountInDB.Leverage)
   {
      isEquivalent = false;
      Log = StringConcatenate("Leverage: [", accountInMT.Login, "][", accountInMT.Leverage, "] != [", accountInDB.Login, "][", accountInDB.Leverage, "]");
      Alert(Log);
   }
   return isEquivalent;
}
void AccountChecking()
{
   int errorno;
   string Log;
   // ���������, ������������ �� �� �� �������
   if (!isConnected())  return;
   // ���������� ������ ����� ��������, ��� ���� ��� �� ����������, ���������� �� �� � ����
   ArrayResize(AccountDB, 0); // ������ ������, ������� ����������� � �������
   // ���������� ������ ������� ��������
   AccountModel account;
   MTGetAccount(account);
   // ���������� ����� ������� ��������, �������� ������������ ������. (��� ���� ������ ������ ���������, ����� ��������� ��� ���� ��4 �������, ��������� � ���, ��� ������� � ����)
   uint totalAccountDB = DBSearchAccount(account);
   if (totalAccountDB == 0)
   {
      Log = "� ���� ��� ��� ������� ��������. ��������� ��� ����.";
      Alert(Log);
      DBInsertAccount(account);
      errorno = db.getLastErrorNumber();
      if (errorno != 0)
      {
         Log = StringConcatenate("�� ������� �������� ������� � ����. ��� ������: ", errorno);
         Alert(Log);
      }
      else
      {
         Log = StringConcatenate("������� ������� ������� � ����");
         Alert(Log);
      }
   }
   else if (totalAccountDB > 1)
   {
      Log = "� ���� ����� ���� �������� ������ � ���� �� ��������. �� id:";
      for (int i = 0; i < totalAccountDB; i++)
      {
         StringAdd(Log, StringConcatenate(" ", AccountDB[i].id));
      }
      StringAdd(Log, StringConcatenate(". �� ����� ������ ������, � id: ", AccountDB[0].id));
      Alert(Log);
   }
   else
   {
      // ���� ����� ���� ������� � ����, �� ���������� �������� ��� � ���������� ������ ���������
      int indexAccountDB = ArrayResize(AccountDB, ArraySize(AccountDB) + 1) - 1;
      AccountDB[indexAccountDB].id = account.id;
      AccountDB[indexAccountDB].Login = account.Login;
      AccountDB[indexAccountDB].Company = account.Company;
      AccountDB[indexAccountDB].Currency = account.Currency;
      AccountDB[indexAccountDB].Name = account.Name;
      AccountDB[indexAccountDB].Server = account.Server;
      AccountDB[indexAccountDB].AccountType = account.AccountType;
      AccountDB[indexAccountDB].Leverage = account.Leverage;
      AccountDB[indexAccountDB].MarginSOMode = account.MarginSOMode;
      AccountDB[indexAccountDB].SOLevel = account.SOLevel;
      
      AccountModel accountMT;
      MTGetAccount(accountMT);
      if (!CompareAccountParametrs(accountMT, account))
      {
         DBUpdateAccount(account.id, accountMT);
      }
   }
}

uint MTGetInstruments(InstrumentModel &instrumentInMT[])
{
   uint totalMTSymbols = SymbolsTotal(false);   ArrayResize(instrumentInMT, totalMTSymbols);
   for (int i = 0; i < totalMTSymbols; i++)
   {
      if (IsStopped())  return 0;
      instrumentInMT[i].id_accounts       = 0;
      instrumentInMT[i].Instrument        = SymbolName(i, false);
      instrumentInMT[i].isMarketWatch     = SymbolInfoInteger(instrumentInMT[i].Instrument, SYMBOL_SELECT);
      instrumentInMT[i].BaseAsset         = SymbolInfoString(instrumentInMT[i].Instrument, SYMBOL_CURRENCY_BASE);
      instrumentInMT[i].MarginAsset       = SymbolInfoString(instrumentInMT[i].Instrument, SYMBOL_CURRENCY_MARGIN);
      instrumentInMT[i].ProfitAsset       = SymbolInfoString(instrumentInMT[i].Instrument, SYMBOL_CURRENCY_PROFIT);
      instrumentInMT[i].SymbolDescription = SymbolInfoString(instrumentInMT[i].Instrument, SYMBOL_DESCRIPTION);
      //instrumentInMT[i].Left3symbolAsset  = StringSubstr(instrumentInMT[i].Instrument, 0, 3);
      //instrumentInMT[i].Right3symbolAsset = StringSubstr(instrumentInMT[i].Instrument, 3, 3);
      instrumentInMT[i].TypeSwap          = SymbolInfoInteger(instrumentInMT[i].Instrument, SYMBOL_SWAP_MODE);
      instrumentInMT[i].SwapBuy           = SymbolInfoDouble(instrumentInMT[i].Instrument, SYMBOL_SWAP_LONG);
      instrumentInMT[i].SwapSell          = SymbolInfoDouble(instrumentInMT[i].Instrument, SYMBOL_SWAP_SHORT);
      instrumentInMT[i].SwapBuyUSD        = NormalizeDouble(SwapUSD(instrumentInMT[i].Instrument, OP_BUY), 2);
      instrumentInMT[i].SwapSellUSD       = NormalizeDouble(SwapUSD(instrumentInMT[i].Instrument, OP_SELL), 2);
      instrumentInMT[i].ContractSize      = NormalizeDouble(SymbolInfoDouble(instrumentInMT[i].Instrument, SYMBOL_TRADE_CONTRACT_SIZE), 2);
      instrumentInMT[i].MarginSymbolUSD   = NormalizeDouble(MarginUSD(instrumentInMT[i].Instrument), 2);
      instrumentInMT[i].digits            = SymbolInfoInteger(instrumentInMT[i].Instrument, SYMBOL_DIGITS);
      instrumentInMT[i].TradeMode         = SymbolInfoInteger(instrumentInMT[i].Instrument, SYMBOL_TRADE_MODE);
      instrumentInMT[i].LastAsk           = SymbolInfoDouble(instrumentInMT[i].Instrument, SYMBOL_ASK);
      instrumentInMT[i].LastBid           = SymbolInfoDouble(instrumentInMT[i].Instrument, SYMBOL_BID);
      instrumentInMT[i].Spread            = SymbolInfoInteger(instrumentInMT[i].Instrument, SYMBOL_SPREAD);
      instrumentInMT[i].SpreadUSD         = NormalizeDouble(SpreadUSD(instrumentInMT[i].Instrument), 2);
      instrumentInMT[i].Volatility        = Volatility(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������� �� ��������� 90 ����
      instrumentInMT[i].Profitability     = Profitability(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������ �� ��������� 90 ����
      instrumentInMT[i].VolatilityUSD     = VolatilityUSD(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������� �� ��������� 90 ����
      instrumentInMT[i].ProfitabilityUSD  = ProfitabilityUSD(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������ �� ��������� 90 ����   
   }
   
   return totalMTSymbols;
}

uint MTGetInstrumentsFloatingParametrs(InstrumentModel &instrumentInMT[])
{
   uint totalSymbols = ArraySize(instrumentInMT);
   for (int i = 0; i < totalSymbols; i++)
   {
      if (IsStopped())  return 0;
      instrumentInMT[i].LastAsk           = SymbolInfoDouble(instrumentInMT[i].Instrument, SYMBOL_ASK);
      instrumentInMT[i].LastBid           = SymbolInfoDouble(instrumentInMT[i].Instrument, SYMBOL_BID);
      instrumentInMT[i].Spread            = SymbolInfoInteger(instrumentInMT[i].Instrument, SYMBOL_SPREAD);
      instrumentInMT[i].SwapBuyUSD        = NormalizeDouble(SwapUSD(instrumentInMT[i].Instrument, OP_BUY), 2);
      instrumentInMT[i].SwapSellUSD       = NormalizeDouble(SwapUSD(instrumentInMT[i].Instrument, OP_SELL), 2);
      instrumentInMT[i].MarginSymbolUSD   = NormalizeDouble(MarginUSD(instrumentInMT[i].Instrument), 2);
      instrumentInMT[i].SpreadUSD         = NormalizeDouble(SpreadUSD(instrumentInMT[i].Instrument), 2);
      instrumentInMT[i].Volatility        = Volatility(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������� �� ��������� 90 ����
      instrumentInMT[i].Profitability     = Profitability(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������ �� ��������� 90 ����
      instrumentInMT[i].VolatilityUSD     = VolatilityUSD(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������� �� ��������� 90 ����
      instrumentInMT[i].ProfitabilityUSD  = ProfitabilityUSD(instrumentInMT[i].Instrument, TimeDiff(TimeCurrent(), 3, -1, PERIOD_MN1), TimeCurrent()); // ������������ �� ��������� 90 ����   
   }
   
   return totalSymbols;
}

int StringArrayBsearch(string &searchArray[], string item)
{
   int index = -1;
   uint total = ArraySize(searchArray);
   for (int i = 0; i < total; i++)
   {
      if (searchArray[i] == item)
      {
         index = i;
      }
   }
   
   return index;
}

void AddUniqueItemForArray(string &destonationArray[], string item)
{
   bool isExecute = false;
   if (StringArrayBsearch(destonationArray, item) != -1) isExecute = true;
   if (!isExecute)
   {
      int index = ArrayResize(destonationArray, ArraySize(destonationArray) + 1) - 1;
      destonationArray[index] = item;
   }
}

int StringArrayBsearchForInstrumentModel(InstrumentModel &searchArray[], string instrument)
{
   int index = -1;
   uint total = ArraySize(searchArray);
   for (int i = 0; i < total; i++)
   {
      if (searchArray[i].Instrument == instrument)
      {
         index = i;
      }
   }
   
   return index;
}

bool CompareInstrument(InstrumentModel &instrumentInMT, InstrumentModel &instrumentInDB)
{
   string Log;
   bool isEquivalent = true;
   // ���������� isMarketWatch
   if (instrumentInMT.isMarketWatch != instrumentInDB.isMarketWatch)
   {
      isEquivalent = false;
      Log = StringConcatenate("isMarketWatch: [", instrumentInMT.Instrument, "][", instrumentInMT.isMarketWatch, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.isMarketWatch, "]");
      Alert(Log);
   }
   // ���������� BaseAsset
   if (instrumentInMT.BaseAsset != instrumentInDB.BaseAsset)
   {
      isEquivalent = false;
      Log = StringConcatenate("BaseAsset: [", instrumentInMT.Instrument, "][", instrumentInMT.BaseAsset, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.BaseAsset, "]");
      Alert(Log);
   }
   // ���������� MarginAsset
   if (instrumentInMT.MarginAsset != instrumentInDB.MarginAsset)
   {
      isEquivalent = false;
      Log = StringConcatenate("MarginAsset: [", instrumentInMT.Instrument, "][", instrumentInMT.MarginAsset, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.MarginAsset, "]");
      Alert(Log);
   }
   // ���������� ProfitAsset
   if (instrumentInMT.ProfitAsset != instrumentInDB.ProfitAsset)
   {
      isEquivalent = false;
      Log = StringConcatenate("ProfitAsset: [", instrumentInMT.Instrument, "][", instrumentInMT.ProfitAsset, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.ProfitAsset, "]");
      Alert(Log);
   }
   // ���������� SymbolDescription
   if (instrumentInMT.SymbolDescription != instrumentInDB.SymbolDescription)
   {
      isEquivalent = false;
      Log = StringConcatenate("SymbolDescription: [", instrumentInMT.Instrument, "][", instrumentInMT.SymbolDescription, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.SymbolDescription, "]");
      Alert(Log);
   }
   // ���������� Right3SymbolAsset
   /*
   if (instrumentInMT.Right3symbolAsset != instrumentInDB.Right3symbolAsset)
   {
      isEquivalent = false;
      Log = StringConcatenate("Right3SymbolAsset: [", instrumentInMT.Instrument, "][", instrumentInMT.Right3symbolAsset, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.Right3symbolAsset, "]");
      Alert(Log);
   }*/
   // ���������� TypeSwap
   if (instrumentInMT.TypeSwap != instrumentInDB.TypeSwap)
   {
      isEquivalent = false;
      Log = StringConcatenate("TypeSwap: [", instrumentInMT.Instrument, "][", instrumentInMT.TypeSwap, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.TypeSwap, "]");
      Alert(Log);
   }
   // ���������� SwapBuy
   if (instrumentInMT.SwapBuy != instrumentInDB.SwapBuy)
   {
      isEquivalent = false;
      Log = StringConcatenate("SwapBuy: [", instrumentInMT.Instrument, "][", instrumentInMT.SwapBuy, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.SwapBuy, "]");
      Alert(Log);
   }
   // ���������� SwapSell
   if (instrumentInMT.SwapSell != instrumentInDB.SwapSell)
   {
      isEquivalent = false;
      Log = StringConcatenate("SwapSell: [", instrumentInMT.Instrument, "][", instrumentInMT.SwapSell, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.SwapSell, "]");
      Alert(Log);
   }
   // ���������� ������ ���������
   if (instrumentInMT.ContractSize != instrumentInDB.ContractSize)
   {
      isEquivalent = false;
      Log = StringConcatenate("ContractSize: [", instrumentInMT.Instrument, "][", instrumentInMT.ContractSize, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.ContractSize, "]");
      Alert(Log);
   }
   // ���������� Digits
   if (instrumentInMT.digits != instrumentInDB.digits)
   {
      isEquivalent = false;
      Log = StringConcatenate("Digits: [", instrumentInMT.Instrument, "][", instrumentInMT.digits, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.digits, "]");
      Alert(Log);
   }
   // ���������� Digits
   if (instrumentInMT.TradeMode != instrumentInDB.TradeMode)
   {
      isEquivalent = false;
      Log = StringConcatenate("TradeMode: [", instrumentInMT.Instrument, "][", instrumentInMT.TradeMode, "] != [", instrumentInDB.Instrument, "][", instrumentInDB.TradeMode, "]");
      Alert(Log);
   }
   
   return isEquivalent;
}

uint DBReadInstruments(uint id_accounts, InstrumentModel &instruments[])
{
   db.select("instruments");
   // ������ ������ � ������ id_accounts
   string where = StringConcatenate("`id_accounts` = '", id_accounts, "'");
   uint num_row = db.readWhere(where);
   if (num_row > 0)
   {
      ArrayResize(instruments, num_row);
      for (int i=0; i < num_row; i++)
      {
         DBGetInstrument(instruments[i], i);
      }
   }
   return num_row;
}
/*
uint DBSearchInstrument(uint id_accounts, string instrument, InstrumentModel &instruments)
{
   db.select("instruments");
   // ������ ������ � ������ id_accounts
   string where = StringConcatenate("`id_accounts` = '", id_accounts, "' AND `instrument` = '", instrument, "'");
   uint num_row = db.readWhere(where);
   if (num_row > 0)
   {
      ArrayResize(instruments, num_row);
      for (int i=0; i < num_row; i++)
      {
         DBGetInstrument(instruments, i);
      }
   }
   return num_row;
}
*/

void DBSetInstrument(InstrumentModel &instrument)
{
   db.set("Instrument",       instrument.Instrument);
   db.set("isMarketWatch",    instrument.isMarketWatch);
   db.set("BaseAsset",        instrument.BaseAsset);
   db.set("MarginAsset",      instrument.MarginAsset);
   db.set("ProfitAsset",      instrument.ProfitAsset);
   db.set("SymbolDescription", instrument.SymbolDescription);
   //db.set("Right3SymbolAsset",instrument.Right3symbolAsset);
   db.set("TypeSwap",         instrument.TypeSwap);
   db.set("SwapBuy",          instrument.SwapBuy);
   db.set("SwapSell",         instrument.SwapSell);
   db.set("SwapBuyUSD",       instrument.SwapBuyUSD);
   db.set("SwapSellUSD",      instrument.SwapSellUSD);
   db.set("ContractSize",     instrument.ContractSize);
   db.set("MarginSymbolUSD",  instrument.MarginSymbolUSD);
   db.set("Digits",           instrument.digits);
   db.set("TradeMode",        instrument.TradeMode);
   db.set("Volatility",       instrument.Volatility);
   db.set("Profitability",    instrument.Profitability);
   db.set("VolatilityUSD",    instrument.VolatilityUSD);
   db.set("ProfitabilityUSD", instrument.ProfitabilityUSD);
}
void DBSetInstrumentFloatingParametrs(InstrumentModel &instrument)
{
   db.set("LastAsk",          instrument.LastAsk);
   db.set("LastBid",          instrument.LastBid);
   db.set("Spread",           instrument.Spread);
   db.set("SwapBuyUSD",       instrument.SwapBuyUSD);
   db.set("SwapSellUSD",      instrument.SwapSellUSD);
   db.set("MarginSymbolUSD",  instrument.MarginSymbolUSD);
   db.set("SpreadUSD",        instrument.SpreadUSD);
   db.set("Volatility",       instrument.Volatility);
   db.set("Profitability",    instrument.Profitability);
   db.set("VolatilityUSD",    instrument.VolatilityUSD);
   db.set("ProfitabilityUSD", instrument.ProfitabilityUSD);
}
void DBGetInstrument(InstrumentModel &instrument, int i)
{
   instrument.id         = (int)       db.get("id", i);
   instrument.id_accounts= (int)       db.get("id_accounts", i);
   instrument.Instrument = (string)    db.get("Instrument", i);
   instrument.isMarketWatch = (int)    db.get("isMarketWatch", i);
   instrument.BaseAsset  = (string)    db.get("BaseAsset", i);
   instrument.MarginAsset= (string)    db.get("MarginAsset", i);
   instrument.ProfitAsset= (string)    db.get("ProfitAsset", i);
   instrument.SymbolDescription  = (string)db.get("SymbolDescription", i);
   //instrument.Right3symbolAsset = (string)db.get("Right3SymbolAsset", i);
   instrument.TypeSwap  =  (int)       db.get("TypeSwap", i);
   instrument.SwapBuy   =  (double)    db.get("SwapBuy", i);
   instrument.SwapSell  =  (double)    db.get("SwapSell", i);
   instrument.SwapBuyUSD=  (double)    db.get("SwapBuyUSD", i);
   instrument.SwapSellUSD= (double)    db.get("SwapSellUSD", i);
   instrument.ContractSize=(double)    db.get("ContractSize", i);
   instrument.MarginSymbolUSD=(double)    db.get("MarginSymbolUSD", i);
   instrument.digits    =  (int)       db.get("Digits", i);
   instrument.TradeMode =  (int)       db.get("TradeMode", i);
   instrument.SpreadUSD =  (double)    db.get("SpreadUSD", i);
   instrument.Volatility = (double)    db.get("Volatility", i);
   instrument.Profitability = (double) db.get("Profitability", i);
   instrument.VolatilityUSD = (double) db.get("VolatilityUSD", i);
   instrument.ProfitabilityUSD = (double) db.get("ProfitabilityUSD", i);
}

void DBUpdateInstrument(uint id_instrument, InstrumentModel &instrument)
{
   db.select("instruments");
   // ������ ������ � ������ id_instrument
   db.read(id_instrument);
   // �������� �������� ���� ������
   DBSetInstrument(instrument);
   // ��������� ������ (��������������)
   db.write();
}
void DBUpdateInstrumentFloationgParametrs(uint id_instrument, InstrumentModel &instrument)
{
   db.select("instruments");
   // ������ ������ � ������ id_instrument
   db.read(id_instrument);
   // �������� �������� ���� ������
   DBSetInstrumentFloatingParametrs(instrument);
   // ��������� ������ (��������������)
   db.write();
}

void DBInsertInstrument(uint id_accounts, InstrumentModel &instrument)
{
   // ��������� ����� ������ � ����
   db.AddNew("instruments");
   // ��������� �������� ���� ������
   db.set("id_accounts", id_accounts);
   DBSetInstrument(instrument);
   // ���������� ������ � ����
   db.write();
}

void DBDeleteInstrument(InstrumentModel &instrument)
{
   db.select("instruments");
   // ������ ������ � ������ id_instrument
   db.Delete(instrument.id);
}

void CheckDifferenceMTAndDB(InstrumentModel &instrumentInMT[], InstrumentModel &instrumentInDB[])
{
   int errorno;
   string Log; // ��������� ������ ��� ������ � ��� ���������
   string instrumentTotal[]; // ������ ��� �������� �������� ������������ �� �������� instrumentInMT � instrumentInDB. �������� ������ ���������� ��������
   
   // ���������� � ������ instrumentTotal ��� ����������� ��������� instrumentInDB. ���� ������ ���������� ��� ���� � ������� instrumentTotal, �� �� �� �����������
   uint totalDBSymbols = ArraySize(instrumentInDB);
   for (int i = 0; i < totalDBSymbols; i++)
   {
      AddUniqueItemForArray(instrumentTotal, instrumentInDB[i].Instrument);
   }
   // ���������� � ������ instrumentTotal ��� ����������� ��������� instrumentInMT
   uint totalMTSymbols = ArraySize(instrumentInMT);
   for (int i = 0; i < totalMTSymbols; i++)
   {
      AddUniqueItemForArray(instrumentTotal, instrumentInMT[i].Instrument);
   }
   
   // �������� ��� �����������, ������� ���� � ���������� instrumentInMT � instrumentInDB. ������ ����� ���������� � �������� ������������ ��������� ���� ��������
   uint totalSymbols = ArraySize(instrumentTotal);
   if (totalSymbols > 0)
   {
      for (int i = 0; i < totalSymbols; i++)
      {
         // ���������� ����� ����������� �� ��
         int indexMT = StringArrayBsearchForInstrumentModel(instrumentInMT, instrumentTotal[i]);
         if (indexMT == -1)
         {
            // ����������� ��� � ��, � ������, ��� ������� ������ ������ instrumentInMT, ���� � ���� ����������� ����������, �������� ������ ��� �� �� � ������� ��� ����� ������� �� ����
            // ��� ����, ��� �� ��������, ������������� instrumentInMT ��� ������� �����, ��������� ��������� ����������� �� ���������
            long isMarketWatch = 0;
            bool isNorm = SymbolInfoInteger(instrumentTotal[i], SYMBOL_SELECT, isMarketWatch);
            if (!isNorm)
            {
               uint error = GetLastError();
               Log = StringConcatenate("�� ������� ����� ���������� ", instrumentTotal[i], " � ���������, ��� ������: ", error);
               Alert(Log);
               // ������ ���������� ���������� ������� �� ����, �.�. �� ����������� � ���������. �������� ��������� ��������� �� �� ������� � ������ ���������� ������ �� ����������
               // �������� ��������
               int indexForDelete = StringArrayBsearchForInstrumentModel(instrumentInDB, instrumentTotal[i]);
               DBDeleteInstrument(instrumentInDB[indexForDelete]);
               continue;
            }
            else
            {
               Log = StringConcatenate("���������� ����������! ��� ������� ������ ������ instrumentInMT. � ��� ����������� ���������� ", instrumentTotal[i], ", � � ��������� �� �����������!");
               Alert(Log);
               continue;
            }
         }
         else
         {
            // ���� ���������� ���� � ��, �� ���������� ����� � ����, ���������� ��
            int indexDB = StringArrayBsearchForInstrumentModel(instrumentInDB, instrumentTotal[i]);
            if (indexDB == -1)
            {
               // �� ����� ���������� � ����, ������ �� ����� � ��� ���������� ��������
               Log = StringConcatenate("� ���� ��� ��� ����������� \"", instrumentTotal[i], "\". ��������� ��� ����...");
               Print(Log);
               DBInsertInstrument(AccountDB[0].id, instrumentInMT[indexMT]);
               errorno = db.getLastErrorNumber();
               if (errorno != 0)
               {
                  Log = StringConcatenate("�� ������� �������� ���������� � ����. ��� ������: ", errorno);
                  Alert(Log);
               }
               else
               {
                  Log = StringConcatenate("���������� ������� ������� � ����");
                  Print(Log);
               }
            }
            else
            {
               // ���������� ���������� � ����, ������ ����� �������� �������� � ��������� �� ���������� � ���� �� �������� ������������. � ������ ������������ ������ �� ����������, ���������� ������� UPDATE � ����
               bool isEquivalent = CompareInstrument(instrumentInMT[indexMT], instrumentInDB[indexDB]);
               if (!isEquivalent)
               {
                  // ����������� �� ������������� ���� �����, ������ ����� �������� ������ � ���� �� ����� �����������
                  Log = StringConcatenate("��������� \"", instrumentInDB[indexDB].Instrument, "\" � ����...");
                  Print(Log);
                  DBUpdateInstrument(instrumentInDB[indexDB].id, instrumentInMT[indexMT]);
                  errorno = db.getLastErrorNumber();
                  if (errorno != 0)
                  {
                     Log = StringConcatenate("�� ������� ������������ ���������� � ����. ��� ������: ", errorno);
                     Alert(Log);
                  }
                  else
                  {
                     Log = StringConcatenate("���������� ������� ����������� � ����");
                     Print(Log);
                  }
               }
            }
         }
      }
   }
   else
   {
      if (totalMTSymbols <= 0)
      {
         Log = StringConcatenate("� ��������� ���������� �����������. �������� � ���� ������ ����� �� ������� �����������. �������� ����������� � ���� ���� ����� � ������������� ��������. �����, ��������� ������ � ��������, �������� �������� �� �������� � ������� �����������");
      }
      else if (totalDBSymbols <= 0)
      {
         Log = StringConcatenate("� ���� ������ ��� ��� ������������ � ����� ������, ������� ��������� ��");
      }
      
      Alert(Log);
      return;
   }
}

void InstrumentFloatingParametrsUpdate()
{
   if (!isConnected())  return;
   ConnectMySql();
   
   // ������ �� ���� �����������, ��� �� ���������
   int size = ArraySize(AccountDB);
   for (int k = 0; k < size; k++)
   {
      InstrumentModel instrumentInDB[]; // ������ �������� ������������ �� ����
      // ������ �����������, ������� �������� � ����
      uint totalDBSymbols = DBReadInstruments(AccountDB[k].id, instrumentInDB);
      // �������� � ��� ��������� ���������� � ��������� (�����, ��������� ���������)
      uint totalMTSymbols = MTGetInstrumentsFloatingParametrs(instrumentInDB);
      // ��������� ����������
      for (int i = 0; i < totalDBSymbols; i++)
      {
         DBUpdateInstrumentFloationgParametrs(instrumentInDB[i].id, instrumentInDB[i]);
      }
   }
}

void InstrumentChecking()
{
   if (!isConnected())  return;
   ConnectMySql();
   
   AccountChecking();
   
   // ������ �� ���� �����������, ��� �� ���������
   int size = ArraySize(AccountDB);
   for (int k = 0; k < size; k++)
   {
      // ������ ������ ������ ����������� ������������ � �������� � ��� ����������
      InstrumentModel instrumentInMT[];
      uint totalMTSymbols = MTGetInstruments(instrumentInMT);
      
      InstrumentModel instrumentInDB[]; // ������ �������� ������������ �� ����
      uint totalDBSymbols = DBReadInstruments(AccountDB[k].id, instrumentInDB);
      
      // ������� ����������� �� ���������, ������� ����������� �� ����. ������ ���������� �� � ���������� ����� ���������
      CheckDifferenceMTAndDB(instrumentInMT, instrumentInDB);
   }
}

double SwapUSD(string symbolName, int orderType)
{
   double swap;
   int    swapType = SymbolInfoInteger(symbolName, SYMBOL_SWAP_MODE);
   string symbolCurrencyProfit = SymbolInfoString(symbolName, SYMBOL_CURRENCY_PROFIT);
   string symbolCurrencyBase = SymbolInfoString(symbolName, SYMBOL_CURRENCY_BASE);
   string symbolCurrencyMargin = SymbolInfoString(symbolName, SYMBOL_CURRENCY_MARGIN);
   double symbolProfitToUSDRate;
   double symbolBaseToUSDRate;
   double symbolMarginToUSDRate;
   double symbolRate;
   double swapSource;
   double symbolContract = SymbolInfoDouble(symbolName, SYMBOL_TRADE_CONTRACT_SIZE);
   double pointValue = SymbolInfoDouble(symbolName, SYMBOL_POINT) * symbolContract;

   int symbolSwapType = SYMBOL_SWAP_LONG;
   ENUM_SYMBOL_INFO_DOUBLE rateType = SYMBOL_ASK;
   if (orderType == OP_BUY || orderType == OP_BUYLIMIT || orderType == OP_BUYSTOP)
   {
      symbolSwapType = SYMBOL_SWAP_LONG;
      rateType = SYMBOL_ASK;
   }
   else if (orderType == OP_SELL || orderType == OP_SELLLIMIT || orderType == OP_SELLSTOP)
   {
      symbolSwapType = SYMBOL_SWAP_SHORT;
      rateType = SYMBOL_BID;
   }
   swapSource = SymbolInfoDouble(symbolName, symbolSwapType);
   symbolRate = SymbolInfoDouble(symbolName, rateType);
   
   int error = RateToUSD(symbolCurrencyProfit, symbolProfitToUSDRate, rateType);
   if (error != 0)
   {
      // ���� ����� ��� ������
   }
   error = RateToUSD(symbolCurrencyBase, symbolBaseToUSDRate, rateType);
   if (error != 0)
   {
      // ���� ����� ��� ������
   }
   error = RateToUSD(symbolCurrencyMargin, symbolMarginToUSDRate, rateType);
   if (error != 0)
   {
      // ���� ����� ��� ������
   }

   switch (swapType)
   {
      case 0:   // � �������
         swap = swapSource * pointValue * symbolProfitToUSDRate;
         break;
      case 1:   // � ������� ������ ������
         swap = swapSource * symbolBaseToUSDRate;
         break;
      case 2:   // � ���������
         swap = (symbolContract * (swapSource / 100) / 360) * symbolRate * symbolProfitToUSDRate;
         break;
      case 3:    // � ������ ��������� �������
         swap = swapSource * symbolMarginToUSDRate;
         break;
   }
   
   return NormalizeDouble(swap, 2);
}

string SymbolFind(string clearInstrument)
{
   string symbol = "-1";
   int symbolTotal = SymbolsTotal(false);
   
   for (int i = 0; i < symbolTotal; i++)
   {
      string instrument = SymbolName(i, false);
      if (StringFind(instrument, clearInstrument) != -1)
      {
         symbol = instrument;
         break;
      }
   }
   
   return symbol;
}

string CommentSwapTable()
{
   string comment = StringConcatenate("����������      ���� �������      ���� �������\n");
   int symbolTotal = SymbolsTotal(false);
   
   for (int i = 0; i < symbolTotal; i++)
   {
      string symbolName = SymbolName(i, false);
      double swapBuy = NormalizeDouble(SwapUSD(symbolName, OP_BUY), 2);
      double swapSell = NormalizeDouble(SwapUSD(symbolName, OP_SELL), 2);
      
      comment = StringConcatenate(comment, symbolName, ":     ", DoubleToString(swapBuy, 2), "     ", DoubleToString(swapSell, 2), "\n");
   }
   
   return (comment);
}

int RateToUSD(string currency, double &rateCurrency, ENUM_SYMBOL_INFO_DOUBLE AskOrBid = SYMBOL_BID)
{
   if (currency == "USD")
   {
      rateCurrency = 1;
      return 0;
   }
   double rate = 0;
   int error = 0;
   int reverse = 1;
   string symbolToUSD = SymbolFind(StringConcatenate(currency, "USD"));
   if (symbolToUSD == "-1")
   {
      symbolToUSD = SymbolFind(StringConcatenate("USD", currency));
      reverse = -1;
   }
   SymbolInfoDouble(symbolToUSD, AskOrBid, rateCurrency);
   error = GetLastError();
   if (error == ERR_UNKNOWN_SYMBOL)
   {
      symbolToUSD = SymbolFind(StringConcatenate("USD", currency));
      SymbolInfoDouble(symbolToUSD, AskOrBid, rateCurrency);
      error = GetLastError();
      if (error == ERR_UNKNOWN_SYMBOL)
      {
         
      }
      else reverse = -1;
   }
   
   if (error == ERR_UNKNOWN_SYMBOL) return(error);
   if (rateCurrency == 0) return (-1);
   
   if (reverse == 1)
   {
      rateCurrency = rateCurrency;
   }
   else
   {
      rateCurrency = 1 / rateCurrency;
   }
   
   return 0;
}

double MarginUSD(string instrument)
{
   double margin = NormalizeDouble(MarketInfo(instrument, MODE_MARGINREQUIRED), 2);
   margin = AccountValueToUSD(margin);
   return NormalizeDouble(margin, 2);
}
double SpreadUSD(string instrument)
{
   double spread = SymbolInfoDouble(instrument, SYMBOL_ASK) - SymbolInfoDouble(instrument, SYMBOL_BID);
   double pointValue = PointValue(instrument);
   spread = AccountValueToUSD(spread * pointValue);
   return NormalizeDouble(spread, 2);
}
double VolatilityUSD(string instrument, datetime startTime, datetime endTime)
{
   double volatility = Volatility(instrument, startTime, endTime);
   double pointValue = PointValue(instrument);
   double volatilityUSD = volatility * pointValue;
   volatilityUSD = AccountValueToUSD(volatilityUSD);
   
   return NormalizeDouble(volatilityUSD, 2);
}
double PointValue(string instrument)
{
   double tickValue = SymbolInfoDouble(instrument, SYMBOL_TRADE_TICK_VALUE);
   double tickSize = SymbolInfoDouble(instrument, SYMBOL_TRADE_TICK_SIZE);
   if (tickSize == 0) return -1;
   
   double pointValue = tickValue / tickSize;
   
   return pointValue;
}

double ProfitabilityUSD(string instrument, datetime startTime, datetime endTime)
{
   double profitability = Profitability(instrument, startTime, endTime);
   
   double pointValue = PointValue(instrument);
   double profitabilityUSD = profitability * pointValue;
   profitabilityUSD = AccountValueToUSD(profitabilityUSD);
   
   return NormalizeDouble(profitabilityUSD, 2);
}

void SymbolRefresh(string instrument, int period, int sleep = 2000)
{
   GetLastError();
   int Command = 33324;  // Refresh
   long chart = ChartOpen(instrument, period);
   Sleep(sleep);
   int error = GetLastError();
   if (error > 0)
   {
      Print("�� ������� ������� ������ ", instrument, ", ��� ������: ", error);
      if (chart > 0)
      {
         if (chart != ChartIDExpert)
         {
            ChartClosed(chart);
         }
      }
      return;
   }
   
   int handle = WindowHandle(instrument, period);
   
   int intWindowHandle = GetParent(handle);
   if (intWindowHandle == 0)
   {
      if (chart != ChartIDExpert)
      {
         ChartClosed(chart);
      }
      return;
   }
   int intWindowHandle_Parent = intWindowHandle;
   
   PostMessageA(intWindowHandle_Parent, WM_COMMAND, Command, 0);
   ChartNavigate(chart, CHART_BEGIN, 1000);
   Sleep(sleep);
   if (chart != ChartIDExpert)
   {
      ChartClosed(chart);
   }
}

void ClosedUnnecessaryCharts()
{
   //--- ���������� ��� ��������������� ��������
   long currChart,prevChart=ChartFirst();
   int i=0,limit=100;
   //Print("ChartFirst = ",ChartSymbol(prevChart)," ID = ",prevChart);
   while(i<limit)// � ��� ��������� �� ������ 100 �������� ��������
   {
      currChart=ChartNext(prevChart); // �� ��������� ����������� ������� ����� ������
      if(currChart<0) break;          // �������� ����� ������ ��������
      //Print(i,ChartSymbol(currChart)," ID =",currChart);
      prevChart=currChart;// �������� ������������� �������� ������� ��� ChartNext()
      i++;// �� ������� ��������� �������
      
      if (currChart != ChartIDExpert)
      {
         ChartClosed(currChart);
      }
   }
}

bool ChartClosed(long window)
{
   int count = 0; // ������� ���������� ������� �������� ��������
   int countClosed = 15; // ����� ���������� ������� �������� �������
   bool closed = false;
   
   do
   {
      closed = ChartClose(window);
      count++;
      Sleep(500);
   }
   while(!closed  &&  count < countClosed);
   
   if (!closed)
   {
      Print("�� ������� ������� ������ � ������� ", window, ". ���� ��������� ", count, "�������.");
   }
   
   return closed;
}

int Rates2(string instrument, datetime startTime, datetime endTime, MqlRates& rate)
{
   ClosedUnnecessaryCharts();
   static string instruments[];
   //static datetime startTimes[];
   static int countRefreshs[];   int countRefreshWarning = 2;
   static datetime timeResetUpdate[];
   int periodResetUpdate = 60 * 60 * 24;  // ������ �������� ���������� ������� ���������� ����� "countRefreshs" �� ����, ��� ����, ��� �� �������� ���������� ��� ��� �������� ������. ���� �� ����������, �� �� ������� ������� �� ���������� ������
   
   int indexStatic = StringArrayBsearch(instruments, instrument);
   if (indexStatic == -1)
   {
      int sizeArray = ArraySize(instruments);
      ArrayResize(instruments, sizeArray + 1);
      //ArrayResize(startTimes, sizeArray + 1);
      ArrayResize(timeResetUpdate, sizeArray + 1);
      ArrayResize(countRefreshs, sizeArray + 1);
      
      instruments[sizeArray] = instrument;
      //startTimes[sizeArray] = 0;
      timeResetUpdate[sizeArray] = 0;
      countRefreshs[sizeArray] = 0;
      
      indexStatic = StringArrayBsearch(instruments, instrument);
   }
   /*
   int barLeft = iBars(instrument, PERIOD_D1) - 1;
   int barRight = 0;
   
   datetime timeLeft = iTime(instrument, PERIOD_D1, barLeft);
   datetime timeRight = iTime(instrument, PERIOD_D1, barRight);
   */
   int countRefresh = 2;
   int count = 0;
   int barLeft;
   int barRight;
   datetime timeLeft;
   datetime timeRight;
   do
   {
      barLeft = iBars(instrument, PERIOD_D1) - 1;
      barRight = 0;
      timeLeft = iTime(instrument, PERIOD_D1, barLeft);
      timeRight = iTime(instrument, PERIOD_D1, barRight);
      
      if (count > 0)
      {
         if (count == 1)
         {
            Print(instrument, ": ���������� ������ ������ ���: ", startTime," < ", timeLeft, " ���  ", timeLeft, " == 0");
         }
         SymbolRefresh(instrument, PERIOD_D1, SleepOpenCharts);
      }
      
      count++;
   }
   while ((startTime < timeLeft  ||  timeLeft == 0)  &&  count < countRefresh  &&  !IsStopped()  &&  countRefreshs[indexStatic] < countRefreshWarning);
   
   if (((startTime < timeLeft  ||  timeLeft == 0)  ||  barLeft < 0)  &&  countRefreshs[indexStatic] < countRefreshWarning)
   {
      Print(instrument, ": ��� ������ �� ������ � ", startTime, " �� ", endTime, ". ��������� ������ ������ �� ������: ", timeLeft, " �� ", timeRight);
      timeResetUpdate[indexStatic] = TimeCurrent();
      //startTimes[indexStatic] = timeLeft;
      countRefreshs[indexStatic]++;
      return -1;
   }
   if (countRefreshs[indexStatic] >= countRefreshWarning)
   {
      if (timeResetUpdate[indexStatic] + periodResetUpdate > TimeCurrent())
      {
         timeResetUpdate[indexStatic] = TimeCurrent();
         countRefreshs[indexStatic] = 0;
      }
      if (timeLeft == 0  ||  barLeft < 0)
      {
         return -1;
      }
      if (startTime < timeLeft)
      {
         startTime = timeLeft;
      }
   }
   
   rate.time = startTime;
   barLeft = iBarShift(instrument, PERIOD_D1, startTime);
   barRight = iBarShift(instrument, PERIOD_D1, endTime);
   rate.open = iOpen(instrument, PERIOD_D1, barLeft);
   rate.close = iClose(instrument, PERIOD_D1, barRight);
   rate.high = iHigh(instrument, PERIOD_D1, iHighest(instrument, PERIOD_D1, MODE_HIGH, barLeft - barRight, barRight));
   rate.low = iLow(instrument, PERIOD_D1, iLowest(instrument, PERIOD_D1, MODE_LOW, barLeft - barRight, barRight));
   
   return 0;
}

int Rates(string instrument, datetime startTime, datetime endTime, MqlRates &rates[])
{
   int error = 0;
   error = CopyRates(instrument, PERIOD_D1, startTime, endTime, rates);
   if (error < 0)
   {
      Print(instrument, error);
      SymbolRefresh(instrument, PERIOD_D1, SleepOpenCharts);
   }
   /*
   for (ENUM_TIMEFRAMES i = PERIOD_CURRENT; i < PERIOD_MN1; i++)
   {
      if (!(i == PERIOD_CURRENT || i == PERIOD_M1 || i == PERIOD_M5 || i == PERIOD_M15 || i == PERIOD_M30 || i == PERIOD_H1 || i == PERIOD_H4 || i == PERIOD_D1 || i == PERIOD_W1 || i == PERIOD_MN1)) continue;
      //--- ������� �������
      int attempts = 0;
      //--- ������ 25 ������� �������� ��������� �� ������� �������
      while (attempts < 5 && (error = CopyRates(instrument, i, startTime, endTime, rates)) < 0) // ������ ������ �� 0 �� count, ����� count ���������
      {
         SymbolRefresh(instrument, i);
         attempts++;
      }
      
      if (error <= 0)
      {
         //--- ���� �� ������� ����������� ����������� ���������� �����
         /*string comm = StringFormat("��� ������� %s �� ������� �������� ���� �� ������ � %s �� %s ����� ��������� %d",
                                      instrument,
                                      error,
                                      TimeToString(startTime),
                                      TimeToString(endTime),
                                      i
                                   );
         //--- ������� ��������� � ����������� �� ������� ���� �������
         Comment(comm);
         continue;
      }
      else  break;
   }*/
   
   return(error);
 //  return error;
}

datetime TimeDiff(datetime startTime, int diff, int diffType = -1, ENUM_TIMEFRAMES period = PERIOD_D1, bool beginingPeriod = true)
{
   diff = diff - 1;
   datetime endTime;
   MqlDateTime startTimeStruct;
   TimeToStruct(startTime, startTimeStruct);
   
   switch(period)
   {
      case PERIOD_M1: startTimeStruct.min = startTimeStruct.min + (diff * diffType); if (beginingPeriod) {  startTimeStruct.sec = 0;   };
      case PERIOD_M5: startTimeStruct.min = startTimeStruct.min + (diff * diffType) * period; if (beginingPeriod) {  startTimeStruct.sec = 0;   };
      case PERIOD_M15: startTimeStruct.min = startTimeStruct.min + (diff * diffType) * period; if (beginingPeriod) {  startTimeStruct.sec = 0; };
      case PERIOD_M30: startTimeStruct.min = startTimeStruct.min + (diff * diffType) * period; if (beginingPeriod) {  startTimeStruct.sec = 0;  };
      case PERIOD_H1: startTimeStruct.hour = startTimeStruct.hour + (diff * diffType); startTimeStruct.min = 0; if (beginingPeriod) {  startTimeStruct.sec = 0;  };
      case PERIOD_H4: startTimeStruct.hour = startTimeStruct.hour + (diff * diffType) * 4; startTimeStruct.min = 0; if (beginingPeriod) {  startTimeStruct.sec = 0; };
      case PERIOD_D1: startTimeStruct.day = startTimeStruct.day + (diff * diffType); if (beginingPeriod) {  startTimeStruct.hour = 0; startTimeStruct.min = 0; startTimeStruct.sec = 0;   };
      case PERIOD_W1: startTimeStruct.day = startTimeStruct.day + (diff * diffType) * 7; if (beginingPeriod) {  startTimeStruct.hour = 0; startTimeStruct.min = 0; startTimeStruct.sec = 0; };
      case PERIOD_MN1: startTimeStruct.mon = startTimeStruct.mon + (diff * diffType); if (beginingPeriod) {  startTimeStruct.day = 0; startTimeStruct.hour = 0; startTimeStruct.min = 0; startTimeStruct.sec = 0;   };
   }
   
   endTime = StructToTime(startTimeStruct);
   
   return endTime;
}

double Volatility(string instrument, datetime startTime, datetime endTime)
{
   MqlRates rates[1];
   ArraySetAsSeries(rates, true);
   if (Rates2(instrument, startTime, endTime, rates[0]) == -1)  return -1; // FAIL
   
   double upPrice;
   double downPrice;
   
   int countBars = ArraySize(rates);
   if (countBars <= 0)  return -1;
   
   for (int i = 0; i < countBars; i++)
   {
      if (rates[i].high > upPrice)  upPrice = rates[i].high;
      if (i == 0) downPrice = rates[i].low;
      if (rates[i].low < downPrice) downPrice = rates[i].low;
   }
   
   return NormalizeDouble(upPrice - downPrice, SymbolInfoInteger(instrument, SYMBOL_DIGITS));
}
double Profitability(string instrument, datetime startTime, datetime endTime)
{
   MqlRates rates[1];
   ArraySetAsSeries(rates, true);
   if (Rates2(instrument, startTime, endTime, rates[0]) == -1)  return -1; // FAIL
   int countBars = ArraySize(rates);
   if (countBars <= 0)  return -1;
   
   double open = rates[countBars - 1].open;
   double close = rates[0].close;
   
   return NormalizeDouble(close - open, SymbolInfoInteger(instrument, SYMBOL_DIGITS));
}

double AccountValueToUSD(double value)
{
   string currency = AccountCurrency();
   double rateToUSD;
   int error = RateToUSD(currency, rateToUSD);
   value = value * rateToUSD;
   return value;
}

string Escape(string strInput)
{
    StringReplace(strInput, "'", "\'");
    return strInput;
}